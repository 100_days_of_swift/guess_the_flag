//
//  ViewController.swift
//  Guess the Flag
//
//  Created by Hariharan S on 01/05/24.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet private weak var button_1: UIButton!
    @IBOutlet private weak var button_2: UIButton!
    @IBOutlet private weak var button_3: UIButton!
    
    // MARK: - Properties

    var score = 0
    var correctAnswer = 0
    var questionsAnswered = 0
    var countries = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "uk", "us"]
        self.configureButtonsLayer()
        self.loadRandomImages()
    }
}

// MARK: - IBAction

private extension ViewController {
    @IBAction func buttonTapped(_ sender: UIButton) {
        self.questionsAnswered += 1
        self.score = sender.tag == self.correctAnswer ? self.score + 1 : self.score - 1
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Score: \(self.score)")
        
        if sender.tag != self.correctAnswer {
            self.presentWrongAnwserAlert(tag: sender.tag)
        }
        
        if self.questionsAnswered == 10 {
            self.presentAlertFor10Questions()
            self.score = 0
            self.questionsAnswered = 0
        }
        self.loadRandomImages()
    }
}

// MARK: - Private Methods

private extension ViewController {
    func configureButtonsLayer() {
        self.button_1.layer.borderWidth = 1
        self.button_2.layer.borderWidth = 1
        self.button_3.layer.borderWidth = 1
        
        self.button_1.layer.borderColor = UIColor.lightGray.cgColor
        self.button_2.layer.borderColor = UIColor.lightGray.cgColor
        self.button_3.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func loadRandomImages(action: UIAlertAction! = nil) {
        self.countries.shuffle()
        self.button_1.setImage(UIImage(named: self.countries[0]), for: .normal)
        self.button_2.setImage(UIImage(named: self.countries[1]), for: .normal)
        self.button_3.setImage(UIImage(named: self.countries[2]), for: .normal)
        self.correctAnswer = Int.random(in: 0...2)
        self.title = self.countries[correctAnswer].uppercased()
    }
    
    func presentWrongAnwserAlert(tag: Int) {
        let wrongAnswerAlert = UIAlertController(
            title: "Wrong Guess",
            message: "Oops, That’s the flag of \(self.countries[tag])",
            preferredStyle: .alert
        )
        
        let continueAction = UIAlertAction(
            title: "Continue",
            style: .default
        )
        wrongAnswerAlert.addAction(continueAction)
        self.present(wrongAnswerAlert, animated: true)
    }
    
    func presentAlertFor10Questions() {
        let scoreAlert = UIAlertController(
            title: "Score Alert",
            message: "After answered 10 questions, Your Score is \(self.score)",
            preferredStyle: .alert
        )
        
        let okAction = UIAlertAction(
            title: "Retry",
            style: .default
        ) { _ in
                self.navigationItem.rightBarButtonItem = nil
        }
        scoreAlert.addAction(okAction)
        
        let exitAction = UIAlertAction(
            title: "Exit",
            style: .destructive
        ) { _ in
            UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
        }
        scoreAlert.addAction(exitAction)
        
        self.present(scoreAlert, animated: true)
    }
}
